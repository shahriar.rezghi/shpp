var indexSectionsWithContent =
{
  0: "bhmprs",
  1: "rs",
  2: "s",
  3: "rs",
  4: "bhmps",
  5: "h"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "variables",
  5: "typedefs"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Variables",
  5: "Typedefs"
};

