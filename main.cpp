// This file is part of the SHPP distribution.
// Copyright (c) 2023 Shahriar Rezghi Shirsavar.
//
// SHPP is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3.
//
// SHPP is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SHPP. If not, see <http://www.gnu.org/licenses/>.

#include <fstream>
#include <iostream>

#include "src/server.h"

using namespace std;

using namespace SHPP;

/// Handle the case where the requested content is not found
Response not_found(const Request &request)
{
    Response response;
    std::ifstream file("website/404.html");
    response.status = 404;
    response.headers["Content-Type"] = "text/html";
    response.body = std::string(std::istreambuf_iterator<char>(file), std::istreambuf_iterator<char>());
    return response;
}

/// Handle all of the incomming requests
Response handler(const Request &request)
{
    if ("/benchmark" == request.path)
    {
        Response response;
        response.body = std::string(615, 'c');
        return response;
    }

    // Get the request path and edit it to serve the content
    auto path = request.path;
    if (path == "/") path = "/index.html";
    path = "website" + path;

    // Remove the stuff after question mark to make the webpages work
    auto question = path.find("?");
    if (std::string::npos != question) path = path.substr(0, question);
    auto end = path.find_last_of(".");

    // If the file does not have a format, return not found
    if (std::string::npos == end) return not_found(request);

    auto format = path.substr(end + 1);
    auto mode = std::ios::in;
    if (format == "jpg" || format == "woff") mode |= std::ios::binary;
    std::ifstream file(path, mode);

    // If the file can't be opened, return not found
    if (!file.is_open()) return not_found(request);

    Response response;
    response.body = std::string(std::istreambuf_iterator<char>(file), std::istreambuf_iterator<char>());

    // Set the proper content type based on file format
    if (format == "jpg")
        response.headers["Content-Type"] = "image/jpeg";
    else if (format == "svg")
        response.headers["Content-Type"] = "image/svg+xml";
    else if (format == "js")
        response.headers["Content-Type"] = "application/javascript";
    else if (format == "css")
        response.headers["Content-Type"] = "text/css";
    else if (format == "html")
        response.headers["Content-Type"] = "text/html";
    else if (format == "txt")
        response.headers["Content-Type"] = "text/plain";
    else if (format == "woff")
        response.headers["Content-Type"] = "application/x-font-woff";

    // Return the response
    return response;
}

int main()
{
    // Create the server with handler, set port and threads numbers, and finally run the server
    Server server(handler);
    server.set_port(8787);
    server.set_threads(8);
    server.run();
    return 0;
}
