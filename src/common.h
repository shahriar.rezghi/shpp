// This file is part of the SHPP distribution.
// Copyright (c) 2023 Shahriar Rezghi Shirsavar.
//
// SHPP is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3.
//
// SHPP is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SHPP. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <map>
#include <stdexcept>
#include <string>

/// Assertion helper macro
#define Assert(expr, msg) \
    if (!static_cast<bool>(expr)) throw std::runtime_error(msg);

namespace SHPP
{
/// Wrapper class of an HTTP request
struct Request
{
    /// Major http version of the request
    int major = 1;

    /// Minor http version of the request
    int minor = 0;

    /// Mapping of the http key:value headers
    std::map<std::string, std::string> headers;

    /// Method of the HTTP request
    std::string method;

    /// Path of the HTTP request
    std::string path;

    /// Body of the HTTP request
    std::string body;
};

/// Wrapper class of an HTTP response
struct Response
{
    /// Status of the HTTP response
    int status = 200;

    /// Mapping of the http key:value headers
    std::map<std::string, std::string> headers;

    /// Body of the HTTP response
    std::string body;
};
}  // namespace SHPP
