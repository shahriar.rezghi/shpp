// This file is part of the SHPP distribution.
// Copyright (c) 2023 Shahriar Rezghi Shirsavar.
//
// SHPP is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3.
//
// SHPP is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SHPP. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include "common.h"

namespace SHPP
{

class Client
{
public:
    inline Client(int client_socket) : client_socket(client_socket) {}

    inline void set_addr_port(const std::string &address, uint16_t port)
    {
        this->address = address;
        this->port = port;
    }
    inline int get_socket() const { return client_socket; }
    inline const Request &get_request() const { return request; }
    inline bool error() const { return state == State::Error; }
    inline bool done() const { return state == State::Done; }
    inline bool writing() const { return state == State::WriteData; }
    inline bool valid() const { return client_socket >= 0; }

    void read();
    void ready_write(Response &response);
    void write();
    void ready_destroy();

private:
    enum State
    {
        ReadHeader,
        ReadBody,
        WriteData,
        Error,
        Done,
    };

    State state = State::ReadHeader;
    uint16_t port = 0;
    int client_socket = -1;
    std::string address, input_data, output_data;
    size_t header_size = 0, body_size = 0, written_size = 0;
    Request request;

    void read_socket();
    void read_header(size_t previous_length);
    void read_body();
};
}  // namespace SHPP
