// This file is part of the SHPP distribution.
// Copyright (c) 2023 Shahriar Rezghi Shirsavar.
//
// SHPP is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3.
//
// SHPP is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SHPP. If not, see <http://www.gnu.org/licenses/>.

#include "server.h"

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/epoll.h>
#include <sys/fcntl.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <unistd.h>

#include <thread>
#include <vector>

#include "client.h"

#define BUFFER 1024

namespace SHPP
{
class Thread
{
public:
    Thread(int server_socket, const Server::Handler &handler)  //
        : server_socket(server_socket), handler(handler)
    {
        epoll_socket = epoll_create1(0);
        Assert(epoll_socket >= 0, "Failed to create epoll socket");

        epoll_event event = {.events = EPOLLEXCLUSIVE | EPOLLIN | EPOLLERR, .data = {.ptr = NULL}};
        auto result = epoll_ctl(epoll_socket, EPOLL_CTL_ADD, server_socket, &event);
        Assert(result >= 0, "Failed to add server socket to epoll socket");
    }
    ~Thread() { close(epoll_socket); }

    void run();

private:
    int server_socket, epoll_socket;
    Server::Handler handler;

    enum class Result
    {
        ShouldClose,
        DontClose,
    };

    void accept();
    Result process(Client &client, uint32_t events);
};

void Thread::run()
{
    epoll_event events[BUFFER];
    Client *clients[BUFFER];
    int client_count = 0;

    while (true)
    {
        int size = epoll_wait(epoll_socket, events, BUFFER, -1);
        Assert(size >= 0, "Epoll wait command failed");

        for (int i = 0; i < size; ++i)
        {
            if (nullptr == events[i].data.ptr)
                accept();
            else
            {
                auto client = static_cast<Client *>(events[i].data.ptr);
                if (Result::ShouldClose != process(*client, events[i].events)) continue;
                client->ready_destroy();
                clients[client_count++] = client;
            }
        }

        for (int i = 0; i < client_count; ++i) delete clients[i];
        client_count = 0;
    }
}

void Thread::accept()
{
    sockaddr addr;
    socklen_t len = sizeof(sockaddr);
    int client_socket = ::accept(server_socket, &addr, &len);
    if (client_socket < 0) return;

    auto result = fcntl(client_socket, F_SETFL, O_NONBLOCK);

    if (result < 0)
    {
        close(client_socket);
        return;
    }

    auto client = new Client(client_socket);

    std::string client_address;
    auto address = (sockaddr_in *)&addr;
    client_address.resize(INET_ADDRSTRLEN);
    inet_ntop(AF_INET, &address->sin_addr, client_address.data(), INET_ADDRSTRLEN);
    client->set_addr_port(client_address, address->sin_port);

    epoll_event _event = {.events = EPOLLET | EPOLLIN | EPOLLRDHUP, .data = {.ptr = client}};
    result = epoll_ctl(epoll_socket, EPOLL_CTL_ADD, client_socket, &_event);

    if (result < 0)
    {
        close(client_socket);
        delete client;
    }
}

Thread::Result Thread::process(Client &client, uint32_t events)
{
    if (!client.valid()) return Result::DontClose;
    if (events & EPOLLERR) return Result::ShouldClose;

    if (events & EPOLLIN)
    {
        client.read();
        if (client.error())
        {
            Response response;
            response.status = 400;
            response.body = "Bad Request";
            client.ready_write(response);
            client.write();
            return Result::ShouldClose;
        }
        else if (client.done())
        {
            const Request &request = client.get_request();
            Response response = handler(request);
            client.ready_write(response);
            client.write();
            if (!client.writing()) return Result::ShouldClose;
            epoll_event _event = {.events = EPOLLET | EPOLLOUT | EPOLLRDHUP, .data = {.ptr = &client}};
            auto result = epoll_ctl(epoll_socket, EPOLL_CTL_MOD, client.get_socket(), &_event);
            Assert(result >= 0, "Failed to change client socket flags in epoll");
        }
        else if (events & EPOLLRDHUP)
            return Result::ShouldClose;
    }

    if (events & EPOLLOUT)
    {
        client.write();
        if (client.writing()) return Result::DontClose;
        return Result::ShouldClose;
    }
    return Result::DontClose;
}

void run_thread(int socket, const Server::Handler &handler) { Thread(socket, handler).run(); }

void Server::run()
{
    Assert(bool(handler) == true, "Handler is uninitialized");
    Assert(threads > 0, "Number of threads can't be zero");

    auto server_socket = ::socket(AF_INET, SOCK_STREAM, 0);
    Assert(server_socket >= 0, "Failed to initialize server socket");

    int option;
    auto result = setsockopt(server_socket, SOL_SOCKET, SO_REUSEADDR, &option, sizeof(option));
    Assert(result >= 0, "Failed to set reuse option on server socket");

    result = fcntl(server_socket, F_SETFL, O_NONBLOCK);
    Assert(result >= 0, "Failed to set nonblock option on server socket");

    sockaddr_in addr = {.sin_family = AF_INET, .sin_port = htons(port)};
    result = inet_pton(AF_INET, address.c_str(), &addr.sin_addr);
    Assert(result == 1, "Failed to set server address and port");

    result = bind(server_socket, (sockaddr *)&addr, sizeof(addr));
    Assert(result >= 0, "Failed to bind server socket");

    result = listen(server_socket, BUFFER);
    Assert(result >= 0, "Failed to listen to server socket");

    std::vector<std::thread> thread_list;
    for (size_t i = 0; i < threads; ++i)  //
        thread_list.emplace_back(run_thread, server_socket, handler);
    for (auto &thread : thread_list) thread.join();
    close(server_socket);
}
}  // namespace SHPP
