// This file is part of the SHPP distribution.
// Copyright (c) 2023 Shahriar Rezghi Shirsavar.
//
// SHPP is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3.
//
// SHPP is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SHPP. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <cstdint>
#include <functional>

#include "common.h"

/// The namespace of the library
namespace SHPP
{
/// Class to create and run the HTTP server
class Server
{
public:
    /// An alias for the handler callback function
    using Handler = std::function<Response(const Request &)>;

    /// Initialize the server with the request handler
    inline Server(const Handler &handler) : handler(handler) {}

    /// Set the ip address of the server
    ///
    /// @param address the address of the server
    inline void set_address(const std::string &address) { this->address = address; }

    /// Set the port of the server
    ///
    /// @param port port of the server
    inline void set_port(const uint16_t &port) { this->port = port; }

    /// Set the request handler function of the server
    ///
    /// @param handler request handler callbakc function
    inline void set_handler(const Handler &handler) { this->handler = handler; }

    /// Set the number of threads of the server
    ///
    /// @param threads number of threads of the server
    inline void set_threads(const size_t &threads) { this->threads = threads; }

    /// Run the HTTP server
    void run();

private:
    std::string address = "0.0.0.0";
    uint16_t port = 8787;
    Handler handler = {};
    size_t threads = 8;
};
}  // namespace SHPP
