// This file is part of the SHPP distribution.
// Copyright (c) 2023 Shahriar Rezghi Shirsavar.
//
// SHPP is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3.
//
// SHPP is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SHPP. If not, see <http://www.gnu.org/licenses/>.

#include "client.h"

#include <picohttpparser.h>
#include <sys/fcntl.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <unistd.h>

namespace SHPP
{
void Client::read_socket()
{
    int size;
    ioctl(client_socket, FIONREAD, &size);
    if (size == 0) return;

    auto prev_size = input_data.size();
    input_data.resize(prev_size + size);
    auto result = recv(client_socket, input_data.data() + prev_size, size, 0);
    if (result != size) state = Error;
}

void Client::read_header(size_t previous_length)
{
    int minor_version;
    phr_header headers[100];
    const char *method, *path;
    size_t length = input_data.size();
    size_t method_length, path_length, num_headers;
    num_headers = sizeof(headers) / sizeof(headers[0]);

    auto result = phr_parse_request(input_data.data(), length, &method, &method_length, &path, &path_length,
                                    &minor_version, headers, &num_headers, previous_length);

    if (result > 0)
    {
        state = State::ReadBody;
        header_size = result;

        request.method = std::string(method, method_length);
        request.path = std::string(path, path_length);
        request.major = 1;
        request.minor = minor_version;

        for (size_t i = 0; i < num_headers; ++i)
        {
            std::string name(headers[i].name, headers[i].name_len);
            std::string value(headers[i].value, headers[i].value_len);
            request.headers[name] = value;
        }

        auto it = request.headers.find("Content-Length");
        if (it != request.headers.end())
            body_size = std::stoul(it->second);  // TODO handle error, case insensitive
        else
        {
            if (request.method == "PATCH" || request.method == "PUT" || request.method == "POST")
                state = State::Error;
            else
                body_size = 0;
        }
    }
    else if (result == -1)
        state = State::Error;
}

void Client::read_body()
{
    if (input_data.size() >= header_size + body_size)
    {
        request.body = input_data.substr(header_size);
        state = State::Done;
    }
}

void Client::read()
{
    size_t previous_length = input_data.size();
    read_socket();
    if (State::Error == state) return;
    if (input_data.size() == previous_length) return;
    if (State::ReadHeader == state) read_header(previous_length);
    if (State::ReadBody == state) read_body();
}

void Client::ready_write(Response &response)
{
    response.headers["User-Agent"] = "SHPP/0.1.0";
    response.headers["Connection"] = "close";
    response.headers["Content-Length"] = std::to_string(response.body.size());
    output_data = "HTTP/1.1 " + std::to_string(response.status) + " \r\n";
    for (const auto &pair : response.headers) output_data.append(pair.first + ":" + pair.second + "\r\n");
    output_data.append("\r\n").append(response.body);
}

void Client::write()
{
    state = WriteData;
    char *data = output_data.data() + written_size;
    auto result = send(client_socket, data, output_data.size() - written_size, MSG_NOSIGNAL);

    if (result < 0)
    {
        state = State::Error;
        return;
    }

    written_size += result;
    if (written_size >= output_data.size()) state = Done;
}

void Client::ready_destroy()
{
    close(client_socket);
    client_socket = -1;
}
}  // namespace SHPP
