# Introduction

This is a mini-project written with C++ using linux kernel calls. This project depends on the `PicoHTTPParser` library. This library is written only to parse the HTTP requests, and does not implement the networking part in anyway. The code also uses an HTML/CSS template and shows this template in its home page. Also, the code uses an HTML code as its 404 page. The source code is inside the `src/` directory. The documentation for the HTTP server can be seen in the `docs/` directory. The HTML related stuff are in the `website/` directory. The `PicoHTTPParser` is inside the `pico/` directory.

# Running the Project

The project can be built using CMake. The steps are shown below:

``` shell
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make
```

The steps shown above will result in an executable called `SHPP` in the `build/` directoy. Running this executable starts the server. Then you can access the home page with [http://localhost:8787/](http://localhost:8787/) and the 404 page can be seen with [http://localhost:8787/A](http://localhost:8787/A) or any other path that doesn't point to a valid file in the `website/` directory. Also, [http://localhost:8787/benchmark](http://localhost:8787/benchmark) can be used to test the benchmark that returns exactly 615 bytes of data in the response body.

# Performance

The server performs really well, and it responds to 30,000 requests per second. Since the project description mentioned 100,000 requests per second, I thought there was a problem with my code.So I checked the performance of the `NGINX` server, and it responded to around 30,000 requests per second. The `NGINX` project is written pretty well, and it has a very high performance. I tested both using `ab`, and here are the results:

My server:

```
➜  ~ ab -n 10000 -c 100 http://localhost:8787/benchmark
This is ApacheBench, Version 2.3 <$Revision: 1901567 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient)
Completed 1000 requests
Completed 2000 requests
Completed 3000 requests
Completed 4000 requests
Completed 5000 requests
Completed 6000 requests
Completed 7000 requests
Completed 8000 requests
Completed 9000 requests
Completed 10000 requests
Finished 10000 requests


Server Software:
Server Hostname:        localhost
Server Port:            8787

Document Path:          /benchmark
Document Length:        615 bytes

Concurrency Level:      100
Time taken for tests:   0.315 seconds
Complete requests:      10000
Failed requests:        0
Total transferred:      6930000 bytes
HTML transferred:       6150000 bytes
Requests per second:    31779.93 [#/sec] (mean)
Time per request:       3.147 [ms] (mean)
Time per request:       0.031 [ms] (mean, across all concurrent requests)
Transfer rate:          21507.32 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    1   0.2      1       2
Processing:     0    2   2.3      2      25
Waiting:        0    1   2.3      1      25
Total:          2    3   2.3      3      27

Percentage of the requests served within a certain time (ms)
  50%      3
  66%      3
  75%      3
  80%      3
  90%      4
  95%      4
  98%      4
  99%     24
 100%     27 (longest request)
```

NGINX server:

```
➜  ~ ab -n 10000 -c 100 http://localhost:8181/
This is ApacheBench, Version 2.3 <$Revision: 1901567 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient)
Completed 1000 requests
Completed 2000 requests
Completed 3000 requests
Completed 4000 requests
Completed 5000 requests
Completed 6000 requests
Completed 7000 requests
Completed 8000 requests
Completed 9000 requests
Completed 10000 requests
Finished 10000 requests


Server Software:        nginx/1.22.1
Server Hostname:        localhost
Server Port:            8181

Document Path:          /
Document Length:        615 bytes

Concurrency Level:      100
Time taken for tests:   0.325 seconds
Complete requests:      10000
Failed requests:        0
Total transferred:      8480000 bytes
HTML transferred:       6150000 bytes
Requests per second:    30727.25 [#/sec] (mean)
Time per request:       3.254 [ms] (mean)
Time per request:       0.033 [ms] (mean, across all concurrent requests)
Transfer rate:          25446.01 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.2      0       3
Processing:     1    3   0.6      3      12
Waiting:        1    3   0.7      3       9
Total:          3    3   0.5      3      12

Percentage of the requests served within a certain time (ms)
  50%      3
  66%      3
  75%      3
  80%      4
  90%      4
  95%      4
  98%      5
  99%      5
 100%     12 (longest request)
```

Also, it is notable that my server is able to handle 10,000 concurrent requests, while `NGINX` fails to handle this large number of requests. Here is the result:

My server:

```
Concurrency Level:      10000
Time taken for tests:   0.431 seconds
Complete requests:      10000
Failed requests:        0
Total transferred:      6930000 bytes
HTML transferred:       6150000 bytes
Requests per second:    23190.83 [#/sec] (mean)
Time per request:       431.205 [ms] (mean)
Time per request:       0.043 [ms] (mean, across all concurrent requests)
Transfer rate:          15694.57 [Kbytes/sec] received
```

NGINX server:

```
Concurrency Level:      10000
Time taken for tests:   0.261 seconds
Complete requests:      10000
Failed requests:        18112
   (Connect: 0, Receive: 0, Length: 9056, Exceptions: 9056)
Total transferred:      800512 bytes
HTML transferred:       580560 bytes
Requests per second:    38283.08 [#/sec] (mean)
Time per request:       261.212 [ms] (mean)
Time per request:       0.026 [ms] (mean, across all concurrent requests)
Transfer rate:          2992.78 [Kbytes/sec] received
```

As can be seen, `NGINX` has failed requests.

# Features

+ The server is multi-threaded, and the number of threads can be set during initializaiton
+ Handles more then 20,000 concurrent connections (the concurrency limit of `ab` is 20,000, so I couldn't test more)
+ Implements the basic `HTTP` protocol, implementing more features would be outside the scope of a mini-project
+ It supports a beautiful home page, and an even more beautiful `not found` page

# License

This project is licenced under `GNU GENERAL PUBLIC LICENSE, Version 3, 29 June 2007`. The license can be found in the `COPYING` file.
